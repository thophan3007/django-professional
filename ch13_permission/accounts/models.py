from django.contrib.auth.models import AbstractUser
from django.db import  models

class CustomUser(AbstractUser):
  def __init__(self, *args, **kwargs):
    super(CustomUser, self).__init__(*args, **kwargs)
    passwd = [field for field in self._meta.fields if field.attname == 'password']
    if passwd:
        passwd[0].blank = True